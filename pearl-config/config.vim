
" This will look for directories containing 'pack/*/start'
set packpath+=${PEARL_PKGVARDIR}

" Default settings
let g:memolist_memo_suffix = "md"
let g:memolist_fzf = 1

nnoremap <Leader>mn  :MemoNew<CR>
nnoremap <Leader>ml  :MemoList<CR>
nnoremap <Leader>mg  :MemoGrep<CR>

