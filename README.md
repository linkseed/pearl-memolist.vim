# memolist.vim for Pearl

simple memo plugin for Vim.

## Details

- Plugin: https://github.com/glidenote/memolist.vim
- Pearl: https://github.com/pearl-core/pearl
